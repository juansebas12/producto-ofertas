export const NewProductSelected = ( index ) => {
    return ( dispatch ) => {
        dispatch( ProductSelected( index ) );
    }
}

export const ProductSelected = ( index) => {
    return { type: "CARGAR_OFERTA", index }
}
