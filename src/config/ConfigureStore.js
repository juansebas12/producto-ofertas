// Redux
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// Reducers
import reducers from './CombineReducers';

export default function configureStore () {
    let store = createStore(reducers, applyMiddleware(thunk));
    return store;
}

