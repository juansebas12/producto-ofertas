import Home from './App/View/Home';
import './App.css';
import { Provider } from 'react-redux';
import configureStore from './config/ConfigureStore';
let store = configureStore()

function App() {
  return (
    <Provider store={store}>
        <Home></Home>
      </Provider>
  );
}

export default App;
