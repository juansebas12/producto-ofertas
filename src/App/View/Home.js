import React from 'react';
import { connect } from 'react-redux';

import OfferSelect from '../Componets/Offert/OfferSelect'
import OfferDetail from '../Componets/Offert/OfferDetail'

import Characteristics from '../Componets/Characteristics'
import Prices from '../Componets/Prices'

import { NewProductSelected } from '../../config/Acctions/ProductAccions'

class Home extends React.Component {


    constructor(props) {
        props.NewProductSelected(0);
        super(props);
    }
    render() {
        return (
            <div style={{ marginLeft: "15%",marginRight: "15%",marginTop: "5%" }}>
                <div className="row">
                    <div className="col-md-3">
                        <OfferSelect
                            oferts={this.props.state}
                            selectProduct={this.props.NewProductSelected}
                        ></OfferSelect>
                    </div>
                    <div className="col-md-3" />
                    <div className="col-md-6">
                        <OfferDetail
                            ofert={this.props.state.products.ofertaSelected}
                        >
                        </OfferDetail>
                    </div>
                    <div className="col-md-3">
                        <Characteristics
                            characteristics={this.props.state.products.ofertaSelected}>
                        </Characteristics>
                    </div>
                    <div className="col-md-3" />
                    <div className="col-md-6">
                        <Prices
                            characteristics={this.props.state.products.ofertaSelected}
                        ></Prices>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        state
    }
}
const mapDispatchToProps = dispatch => {
    return {
        NewProductSelected: (Product) => {
            dispatch(NewProductSelected(Product))
        },
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Home);