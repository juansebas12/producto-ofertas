import React from 'react';

const Characteristics = (props) => {
    if (props.characteristics.versions != undefined) {
        return (
            <React.Fragment>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Characteristics</th>
                        </tr>
                    </thead>
                    <tbody>
                            {
                                props.characteristics.versions[0].characteristics.map((characteristic, index) =>
                                <tr key={index}>
                                    <th scope="row">{characteristic.versions[0].name}</th>
                                </tr>
                                )
                            }
                    </tbody>
                </table>
            </React.Fragment>
        )
    } else {
        return (<div></div>)
    }
}
export default Characteristics;