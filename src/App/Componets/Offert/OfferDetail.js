import React from 'react';

const OfferSelect = (props) => {
    if (props.ofert.versions != undefined) {
        return (
            <React.Fragment>
                <div className="card">
                    <h6>Id oferta : {props?.ofert?.versions[0].id}</h6>
                    <h6>Nombre : {props?.ofert?.versions[0].name}</h6>
                </div>
            </React.Fragment>
        )
    } else {
        return (<div></div>)
    }
}
export default OfferSelect;