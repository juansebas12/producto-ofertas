import React from 'react';

const OfferSelect = (props) => {
    function change(event) {
        props.selectProduct(event.target.value);
    }
    return (
        <React.Fragment>
            <div className="card">
                <h5>Ofertas</h5>
                <select onChange={change} className="form-select" aria-label="Default select example" defaultValue="0">
                    {
                        props.oferts.products.ofertas.map((ofert, index) =>
                            <option key={index} value={index}>{index + 1}</option>
                        )
                    }
                </select>
            </div>
        </React.Fragment>
    )
}
export default OfferSelect;
