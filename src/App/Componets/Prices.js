import React from 'react';

const Prices = (props) => {
    if (props.characteristics.versions != undefined) {
        console.log(props.characteristics.versions[0].productOfferingPrices)
        return (
            <React.Fragment>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Nombre Precio</th>
                            <th scope="col">Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                            {
                                props.characteristics.versions[0].productOfferingPrices.map((price, index) =>
                                <tr key={index}>
                                    <th scope="row">{price.versions[0].name}</th>
                                    <th scope="row">{price.versions[0].price.amount}</th>
                                </tr>
                                )
                            }
                    </tbody>
                </table>
            </React.Fragment>
        )
    } else {
        return (<div></div>)
    }
}
export default Prices;